import time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import yaml
import logging

CONFIG_FILE = '../bbadmin/config.cfg'

# Initial setup
try:
    with open(CONFIG_FILE, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
except IOError as e:
    logging.error("Config file not found!")
    logging.error("Exception: %s" % str(e))
    sys.exit(1)

timestamp = int(time.time())
mac = '00:11:22:33:44:55'

print('sending', timestamp, 'for', mac)
publish.single('/'.join(['hacklab', 'presence', mac]),
        payload=str(timestamp),
        hostname=cfg['mqtt-server'],
        port=cfg['mqtt-port'],
        auth={'username': cfg['mqtt-username'], 'password': cfg['mqtt-password']},
        protocol=mqtt.MQTTv311)
