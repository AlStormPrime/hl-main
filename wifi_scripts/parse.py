import csv
data = {}
time = set([])

# sqlite> .headers off
# sqlite> .mode csv
# sqlite> .output "db2.csv"
# sqlite> select datetime(time + 90907200, 'unixepoch') as dt, mac, idle, time from records order by dt;

for line in csv.reader(open('db2.csv')):
    data.setdefault(line[1], set([])).add(line[0])
    time.add((int(line[3]), line[0]))

time = sorted(time)
for mac, seq in data.iteritems():
    print '>' * 80
    print mac
    active = False
    start = None
    start_ts = None
    for i in range(len(time)):
        ts, dt = time[i]
        if dt in seq:
            if not active:
                if start_ts is not None:
                    print 'gap ~%.2f hours' % ((ts-start_ts)/3600.)
                start = dt
                start_ts = ts
                active = True
        else:
            if active:
                print start, '->', dt, '(~%.2f hours)' % ((ts-start_ts)/3600.)
                active = False
                start_ts = ts
    if active:
        print start, '-> still here'


# Sequential diffs:
#spectre = {}
#for mac, seq in data.iteritems():
#    for i in range(len(seq) - 1):
#        v = seq[i+1][1]-seq[i][1]
#        spectre[v] = spectre.get(v, 0) + 1
#for i in sorted(spectre.keys()):
#    print i, spectre[i]
# Result:
#59 37
#60 19846
#61 37
#120 7
#240 14
#300 6
#360 1
#420 1
#480 2
#600 1
#660 1
#720 2
#780 1
#1380 1
#1500 1
#6060 1
#8940 1
#50761 1
#54900 1
#59220 1
#60660 1
#63540 1
#64080 1
#70080 1
#79560 1
#86520 1
#93060 1
#143280 1
#153840 1
#155220 1
#156899 1
#171900 1
#254040 1
