<?php
/*
Plugin Name: Wifi presence monitor
Description: The code was based on this example widget:  http://wordpress.org/plugins/kv-simple-subscription/.
Author: Artem Synytsyn <a.synytsyn@gmail.com>
Version: 1.0
Author URI: http://hacklab.kiev.ua
*/

add_action('init', 'register_shortcode_update_users');

function register_shortcode_update_users()
{
    add_shortcode('wifi_update_users', 'wifi_update_users_fn');
}

class Wifi_Presence_widget extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'wifi_presence_widget',
            'description' => 'Widget for wifi presence system',
        );
        parent::__construct('my_widget', 'Wifi Presence', $widget_ops);
    }

    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        if (! empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        /*if (! empty($instance['password'])) {
            echo $args['before_password'] . apply_filters('widget_title', $instance['pass']) . $args['after_password'];
        }*/
        do_action('wifi_update_user_info', $instance['pass'], $instance['closed_banner']);
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {
        $title = ! empty($instance['title']) ? $instance['title'] : esc_html__('New title', 'text_domain');
        $pass = $instance['pass'];
        $closed_banner = $instance['closed_banner'];?>
  		<p>
  		<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_attr_e('Title:', 'text_domain'); ?></label>
  		<input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>">
      <label for="<?php echo esc_attr($this->get_field_id('pass')); ?>"><?php esc_attr_e('Password:', 'text_domain'); ?></label>
  		<input class="widefat" id="<?php echo esc_attr($this->get_field_id('pass')); ?>" name="<?php echo esc_attr($this->get_field_name('pass')); ?>" type="text" value="<?php echo esc_attr($pass); ?>">
      <label for="<?php echo esc_attr($this->get_field_id('closed_banner')); ?>"><?php esc_attr_e('We Are Closed Banner:', 'text_domain'); ?></label>
  		<input class="widefat" id="<?php echo esc_attr($this->get_field_id('closed_banner')); ?>" name="<?php echo esc_attr($this->get_field_name('closed_banner')); ?>" type="text" value="<?php echo esc_attr($closed_banner); ?>">
  		</p>
  		<?php
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     *
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        // processes widget options to be saved
        //$instance = array();
        $instance  = $old_instance;

        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['pass'] = strip_tags( $new_instance['pass']);
        $instance['closed_banner'] = strip_tags( $new_instance['closed_banner']);

        return $instance;
    }
}

add_action('widgets_init', function () {
    register_widget('Wifi_Presence_widget');
});

add_action('wifi_update_user_info', 'wifi_update_users_fn', 10, 2);

function wifi_update_users_fn($password, $banner)
{
    add_option("user_list", 'none');
    add_option("last_update", 'none');
    $array = explode(',', get_option("user_list")); // User list is separated by comma
    $last_update = get_option("last_update");
    $icon_size = 50;
    echo "<style>
          .img-circle {border-radius: 50%;}
          td.min {
              width: $icon_size\px;
              white-space: nowrap;
              text-align: left
          }
          </style>";
    if (get_option("user_list") != '') {
      echo "<table style='border:none;'>";
      foreach ($array as $value) {
          echo "<tr>";
          echo "<td style='border:none;' class='min'><img class='img-circle' src='https://robohash.org/$value.png?size=$icon_size x$icon_size'></td>";
          echo "<td style='border:none; vertical-align: middle;' align='left'>$value</td>";
          echo "</tr>";
      }
      echo "</table>";
    } else {
      echo "<img src='$banner'>";
    }
    $last_update_result = gmdate("H:i:s", time() - $last_update);
    echo "<p>Last update was $last_update_result ago";

    if ('POST' == $_SERVER['REQUEST_METHOD'] && ($_POST['password'] == $password)) {
        update_option("user_list", $_POST['user_list']);
        //update_option("last_update", date('d-m-Y h:i:s a', time()));
        update_option("last_update", time());
    } ?>
<?php
}
?>
