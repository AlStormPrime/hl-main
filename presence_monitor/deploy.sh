#!/usr/bin/env sh

CONFIG_DIR="/usr/share/presence"


ssh -o "PubkeyAuthentication no" "root@""$1" "mkdir -p $CONFIG_DIR"
scp -o "PubkeyAuthentication no" ./presence.cfg "root@""$1":"$CONFIG_DIR"
scp -o "PubkeyAuthentication no" ./presence "root@""$1":/etc/init.d/
scp -o "PubkeyAuthentication no" ./presenced "root@""$1":/usr/bin/

ssh -o "PubkeyAuthentication no" "root@""$1" "/etc/init.d/presence start"
ssh -o "PubkeyAuthentication no" "root@""$1" "/etc/init.d/presence enable"
