Installation
------------

### Install the MQTT client

Install the packages

- `mosquitto-client`
- `coreutils-nohup`

with either luci or opkg.

### Deploy the scripts

On your host machine

```bash
$ sh ./deploy.sh <ROUTER_IP> 
```

You can check if service is up by executing

```bash
$ ssh root@<ROUTER_IP> "/etc/init.d/presence enabled && echo on"
```

Usage
-----

The next settings are configurable in `/usr/share/presence/presence.cfg`:

- MQTT_SERVER - broker's IP address
- MQTT_SERVER_PORT - broker's IP port
- MQTT_ID - a message ID
- MQTT_TOPIC - a message topic
- UPDATE_INTERVAL - if there are no events during this interval 
publish a list of connected devices (seconds)
- MQTT_USER - user 
- MQTT_PASS - password


One can find an example of configuration in `presence.cfg.example`.
