Big Balls Admin Panel
===================

The goal of this webtool is provide basic management capabilities for hackerspaces, like:

1. Presence management basing on MAC address monitoring
2. RFID access system management
3. Payments monitoring
4. Internal information storage(wiki based)

Only for cool guys, only for big balls!:)

Installation
============
1. Install virtualenv in project's directory:
	```sh
    $ virtualenv hackadmin
    ```
2. Install required packages:
	```sh
    $ pip install -r requirements.txt
    ```
3. Run app:
	```sh
    $ python application.py
    ```
Also, running of the code currently requires previously created database:
	```sh
    $ sqlite3 macdatabase.db
    $ create table users (id integer primary key, mac text, nick text);
    $ .quit
    ```

Configuration
=============

Currently config is stored in YAML file. Example of config:

```
# Example config file
---
data:
    mac-db-path: macdatabase.db
    visitors-db-path: visitors.db
    latest-key-file: key.txt
logging:
    debug: Yes
    logfile: log.txt
    logsize_kb: 1000
    rolldepth: 3
mqtt:
    username: wahnfosn
    password: 8P7oll9SD0l5
    server: m10.cloudmqtt.com
    port: 16860
    presence-topic: /some/topic/
    visitors-topic: /some/topic/
wordpress:
    server: http://server.com
    passwd: secret
```

path to config file is set in `applicaiton.py`. By default, config file name is `config.cfg`

Common problems
===============
1. On some systems there is error:
	```sh
    NameError: name 'PROTOCOL_SSLv3' is not defined
    ```
Solution: edit `/home/<username>/.local/lib/python2.7/site-packages/gevent/ssl.py` and
replace `PROTOCOL_SSLv3` to `PROTOCOL_SSLv23`

DEPLOYMENT ON TARGET SYSTEM
===========================

###Linkit smart Openwrt example

Copy bbadmin somewhere. We assume that we put it to `/root/stations/server`:

Deployment consists of three main steps:
1. Adding python scripts to `procd`. This is Openwrt process manager. 
	* Create file `/etc/init.d/macstations`:
		```
        root@mylinkit:/etc/init.d# cat macstations 
        #!/bin/sh /etc/rc.common

        START=99
        STOP=0

        USE_PROCD=1
        SCRIPT="/root/stations/server/application.py"

        start_service() {
            procd_open_instance
            procd_set_param command python "$SCRIPT"
            # procd_set_param stdout 1
            # procd_set_param stderr 1
            procd_close_instance
        }
        ```
        Just for testing: run `python /root/stations/server/application.py`. If everything is ok, you will get no errors in console.
        What else we need to do:
        * Add functionality for restarting webapplication if some crash occurs.
        * There is some problem with config file. Since our working dir is root, application could not find config file. We need to specify full path there! Bad, bad, bad!!!
        
    Ok, now it will start our python webapplication at every system bootup at 127.0.0.1:5000. At this stage webapplication will be **NOT** accessible from local network.

2. Installation **nginix** webserver instead of **uhttpd** in reverse proxy mode. It will pass all http requests from local network to localhost, where our webapp runs.
	* Install `nginix` with command `opkg install nginix`
	* Disable `uhttpd` since it conflicts with nginix: 
		`/etc/init.d/uhttpd stop`
        `/etc/init.d/uhttpd disable`
    * Add to `/etc/nginx/nginix.conf` file `include /etc/nginx/sites-enabled/*;` into `http` section:
    ```
    http {
		include       mime.types;
		include /etc/nginx/sites-enabled/*;
	.... A lot of other stuff ....
    ```
	* `mkdir /etc/nginx/sites-enabled/`
	* `mkdir /etc/nginx/sites-available/`
	* `ln -s /etc/nginx/sites-available/flask_project /etc/nginx/sites-enabled/bbadmin_project`
	* Contains of `/etc/nginx/sites-enabled/bbadmin_project`:
        ```
        server {
            listen 80;
            server_name bbadmin_server;

            location / {
                proxy_pass http://127.0.0.1:5000;
                auth_basic "Restricted Content";
                auth_basic_user_file /etc/nginx/.htpasswd;
            }

            location /socket.io {
                proxy_http_version 1.1;
                proxy_buffering off;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "Upgrade";
                proxy_pass http://127.0.0.1:5000/socket.io;
            }
    	}
        ```
3. Restrict access to our webpage. We need to create `/etc/nginx/.htpasswd` file. Password file has to be generated with special utility: `htpasswd` or any online tools(ex. http://www.htaccesstools.com/htpasswd-generator/). Actuall, this is just MD5 enctypted file, but anyway:
```
htpasswd -n admin #admin - example name of user
```

as result, you will get output of your file.
Place contains into `/etc/nginx/.htpasswd` file.

So, that's all, restart device or services with commands:
```
$ /etc/init.d/macstations restart
$ /etc/init.d/nginix restart
```

Development
===========

Everything is in very early development stage, preliminary TODO list is:
* MQTT functionality:
    - [x] Getting list of active Wifi connections
    - [x] Parsing and updating current state of connections
    - [x] Long term monitoring of connections, to make them not to "disappear" on short-term disconnections
* Datababase:
    - [x] Implement addition, deletion and modification of database
    - [ ] Create database from code
* Layout problems:
    - [x] Footer problem - now it looks akward
    - [x] Table layout: height of rows and size of buttons is not constant
* General problems:
    - [ ] Code review
