#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Core of Hacklab Admin Panel

@author: Artem Synytsyn
"""

from flask import Flask, render_template, request
from flask_socketio import SocketIO
import paho.mqtt.client as mqtt
import time
import json
import yaml
import sys
import os
import database
import logging
from logging.handlers import RotatingFileHandler
from collections import namedtuple
from threading import Thread
from os.path import getmtime
import datetime
import sqlite3
import urllib2
import requests
import txt_log_reader

# Configuration file
CONFIG_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.cfg')

# Initial setup
try:
    with open(CONFIG_FILE, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
except IOError as e:
    logging.error("Config file not found!")
    logging.error("Exception: %s" % str(e))
    sys.exit(1)

USER_DB = cfg['data']['mac-db-path']
VISITORS_DB = cfg['data']['visitors-db-path']
LATEST_KEY_FILE = cfg['data']['latest-key-file']
PHP_SERVER = cfg['wordpress']['server']
PHP_PASSWD = cfg['wordpress']['passwd']
PHP_TIMEOUT = 1

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
logger = logging.getLogger(__name__)
if cfg['logging']['debug'] is True:    
    app.config['DEBUG'] = True
    logging.basicConfig(level=logging.DEBUG)
    # Create logger to be able to use rolling logs    
    logger.setLevel(logging.DEBUG)
    log_handler = RotatingFileHandler(cfg['logging']['logfile'],mode='a',
                                      maxBytes=int(cfg['logging']['logsize_kb'])*1024,
                                      backupCount=int(cfg['logging']['rolldepth']),
                                      delay=0)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)


socketio = SocketIO(app)
mactable = database.MacTable(USER_DB)

#---------------------------------------------------------------------------
#   Misc functions
#--------------------------------------------------------------------------- 

def get_latest_key_info():
    with open(LATEST_KEY_FILE, 'r') as f:
        key_value = f.read()
    # Getting modification datetime
    mod_time = getmtime(LATEST_KEY_FILE)
    mod_time_converted = datetime.datetime.fromtimestamp(
            mod_time).strftime('%Y-%m-%d %H:%M:%S')
    return ("%s updated at: %s" % (key_value, mod_time_converted))

#---------------------------------------------------------------------------
#   Web backend related stuff
#--------------------------------------------------------------------------- 

def send_log_message(message):
    """ Send log message to frontend """

    socketio.emit('newmessage', message, namespace='/log')


def send_table():
    """ Send table data to frontend """
    everything = set(context.online_users.keys())

    for user in mactable.data:
        everything.discard(user['mac'])

    req_string = ','.join([user['nick'].encode('utf8')
                           for user in mactable.data
                           if user['status'] == 'online'])
    logger.info("Sending data to WordPress server")
    logger.error(req_string)
    r = requests.post(PHP_SERVER,
                  data={'password': PHP_PASSWD, 'user_list': req_string},
                  headers={"User-Agent":"Mozilla/5.0"})
    logger.info("Sent string: %s, status code: %d", req_string, r.status_code)
    
    logger.info("Sending data to broker")
    mqtt_client.publish(cfg['mqtt']['visitors-topic'], req_string) 

    other_data = [{'mac': mac, 'status': 'online', 'nick': '<unknown>'}
                  for mac in everything]
    json_sample_data = json.dumps(mactable.data + other_data)
    logger.info("Sending data to local webinterface table")
    socketio.emit('tabledata', json_sample_data, namespace='/tablemgr')
    logger.info('Send table completed')


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/rfid', methods=['GET', 'POST'])
def rfid_page():
    db = sqlite3.connect(VISITORS_DB)
    cursor = db.execute('SELECT * FROM users')
    all_column_names = list(description[0] for description in
                            cursor.description)
    
    # Get column names for devices, needed access control. We assume that first
    # three columns are id, name and key.
    access_info_columns = all_column_names[3:]
    cursor = db.execute('SELECT id, name, key FROM users')
    user_info = cursor.fetchall()
    cursor = db.execute('SELECT %s FROM users'
                        % ','.join(access_info_columns))
    user_access_info = cursor.fetchall()
    template_data = zip(user_info, user_access_info)

    # Updating latest key information
    latest_key_info = get_latest_key_info()
    # Parsing data from frontend: editing access, user additioin and deletion
    if request.method == 'POST':
        operation = request.form.getlist('operation')[0]
        if operation == 'edit':
            user_id = request.form.getlist('id')[0]
            user_device = request.form.getlist('device')[0]
            user_state = request.form.getlist('state')[0]
            if user_state == 'true':
                user_state = '1'
            elif user_state == 'false':
                user_state = '0'

            logger.info('Updated user info: %s, %s, %s' % (user_id,
                         access_info_columns[int(user_device)],
                         user_state))
            command = "UPDATE users SET %s = '%s' WHERE id = %s" \
                % (access_info_columns[int(user_device)], user_state,
                   user_id)
            cursor.execute(command)
            db.commit()
        elif operation == 'delete':
            user_id = request.form.getlist('id')[0]
            user_device = request.form.getlist('device')[0]
            user_state = request.form.getlist('state')[0]
            cursor.execute('DELETE FROM users WHERE id=?', (user_id, ))
            db.commit()
            logger.info('User deleted, id: %s' % user_id)
        elif operation == 'add':

            user_name = request.form.getlist('nick')[0]
            user_key = request.form.getlist('key')[0]
            cursor.execute('INSERT INTO users(name, key) VALUES(?, ?)',
                           (user_name, user_key))
            db.commit()
            logger.info('User added: %s, %s' % (user_name, user_key))

    return render_template('rfid.html', data=template_data,
                           column_names=all_column_names,
                           latest_key_info=latest_key_info)
@app.route("/log_viewer")
def log_reader_wrapper():
    return txt_log_reader.render_logs_to_html()


@socketio.on('connect', namespace='/tablemgr')
def init_table_state():
    send_table()
    logger.info('Table namespace connected')


@socketio.on('connect', namespace='/log')
def init_log_state():
    logger.info('Log namespace connected')


@socketio.on('adduser', namespace='/tablemgr')
def add_new_user(message):
    logger.debug('Add new user event: %s' % message)

    mactable.add_user(message['nickname'], message['macaddress'])

    send_table()
    send_log_message('Added new user: %s, MAC address: %s'
                     % (message['nickname'], message['macaddress']))

@socketio.on('deleteuser', namespace='/tablemgr')
def delete_user(message):
    logger.debug('Delete user event: %s' % message)
    mactable.delete_row(int(message['id']))
    
    send_table()
    send_log_message("User deleted, id: %s"  % message['id'])

@socketio.on('edituser', namespace='/tablemgr')
def edit_user(message):
    logger.debug('Edit user event: %s' % message)
    mactable.edit_row(int(message['id']), message['mac'], message['nick'])
    
    send_table()
    send_log_message("User info edited, id: %s; new nickname: %s, new MAC: %s"
                     % (message['id'], message['mac'], message['nick']))

@socketio.on('uploadTable', namespace='/tablemgr')
def upload_table(message):
    logger.debug('Resend table data')
    send_table()

#---------------------------------------------------------------------------
#   MQTT related stuff
#---------------------------------------------------------------------------  

mqtt_client = mqtt.Client()

# Connection history context
FLUSH_TIME = 10 # How often to flush user data to frontend
OFFLINE_TIME = 60 * 3 # We consider user to be offline after this many seconds of silence
SHORT_OFFLINE_TIME = 10
context = namedtuple('Context', ['latest_flush_time', 'online_users'])
context.latest_flush_time = 0
context.online_users = {} # {<mac> -> <last update time>}

def on_mqtt_connect(client, userdata, flags, rc):
    logger.info("MQTT connect occured. Result code %d", rc)
    if rc == 0:
        logger.info("Connection successful")
    elif rc == 1:
        logger.info("Connection refused - incorrect protocol version")
    elif rc == 2:
        logger.info("Connection refused - invalid client identifier")
    elif rc == 3:
        logger.info("Connection refused - server unavailable")
    elif rc == 4:
        logger.info("Connection refused - bad username or password")
    elif rc == 5:
        logger.info("Connection refused - not authorised")
    else:
        logger.info("Unknown connection error")
    client.subscribe(cfg['mqtt']['presence-topic'])
    logger.info("Subscribed to %s topic", cfg['mqtt']['presence-topic'])

def on_mqtt_disconnect(client, userdata, rc):
    logger.info("MQTT disconnect occured. Result code %d", rc)
    if rc != 0:
        logger.info("Unexpected disconnection.")
    else:
        logger.info("Disconnection occured as responce to disconnect() call")

# The callback for when a PUBLISH message is received from the server.
def on_mqtt_message(client, userdata, msg):
    logger.info("MQTT message received")
    macaddr = msg.topic.split('/')[-1]
    receive_time = time.time()
    logger.info("MAC: %s, User: %s", macaddr, mactable.find_user_by_mac(macaddr))
    # Check if we've received new MAC and handle it
    if not context.online_users.has_key(macaddr):
        send_log_message("Connected: MAC Address: %s, User: %s" %
                (macaddr, mactable.find_user_by_mac(macaddr)))
        logger.info("New MAC connected: %s, User: %s",
                     macaddr, mactable.find_user_by_mac(macaddr))
    # Check if some of previous clients became disconnected
    new_online = {}
    for mac, last_update in context.online_users.iteritems():
        if receive_time - last_update > OFFLINE_TIME:
            send_log_message("Disconnected: MAC Address: %s, User: %s" %
                  (mac, mactable.find_user_by_mac(mac)))
            mactable.set_user_status(mac, 'offline')
            logger.info("MAC disconnected: %s, User: %s",
                     mac, mactable.find_user_by_mac(mac))
        else:
            new_online[mac] = last_update
    context.online_users = new_online
    # Update time and status for all connected clients
    context.online_users[macaddr] = receive_time
    mactable.set_user_status(macaddr, 'online')
    # Send data to frontend
    if receive_time - context.latest_flush_time > FLUSH_TIME:
        send_table()
        context.latest_flush_time = receive_time



if __name__ == '__main__':
    mqtt_client.on_connect = on_mqtt_connect
    mqtt_client.on_disconnect = on_mqtt_disconnect
    mqtt_client.on_message = on_mqtt_message
    
    mqtt_client.username_pw_set(cfg['mqtt']['username'], cfg['mqtt']['password'])
    mqtt_client.connect(cfg['mqtt']['server'], cfg['mqtt']['port'], 60)
    mqtt_client.loop_start()

    socketio.run(app)

			
