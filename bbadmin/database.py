#!/usr/bin/env python2
# -*- coding: utf-8 -*-
""" Dababase utils for Hacklab Admin Tools

@author: Artem Synytsyn
"""

import sqlite3

class MacTable(object):

    """MAC Address table wrapper.

    Currently, this class requires specially generated database with command:
        $ create table users (id integer primary key, mac text, nick text)

    Todo:
        * Make table generation without sqlite3 console
    """

    def __init__(self, db_filename):
        self.database = sqlite3.connect(db_filename, check_same_thread=False)
        self.cursor = self.database.execute('select * from users')
        self.columns = tuple(description[0] for description in self.cursor.description)
        
        self.cursor.execute('SELECT * FROM users')
        raw_data = self.cursor.fetchall()
        converted_data = []
        # Convert tuples to dictionaries for simpler handling on JS side
        for data in raw_data:
            t = dict(zip(self.columns, data))
            # Assume that all statuses are offline at init stage
            t['status'] = 'offline'
            converted_data.append(t)            
        self.data = converted_data
    
    def __find_index_by_id__(self, id_value):
        for index, t in enumerate(self.data):
            if t["id"] == id_value:
                return index
        return None
                
    def find_user_by_mac(self, macaddress):
        self.cursor.execute("SELECT nick FROM users WHERE mac LIKE '%%%s%%'"
                            % macaddress)
        entry = self.cursor.fetchone()
        self.database.commit()
        if entry is None:
            return "unknown"
        else:
            return entry[0]
    
    def find_id_by_mac(self, macaddress):
        self.cursor.execute("SELECT id FROM users WHERE mac LIKE '%%%s%%'"
                            % macaddress)
        entry = self.cursor.fetchone()
        self.database.commit()
        if entry is None:
            return None
        else:
            return entry[0]  

    def set_user_status(self, macaddress, status):
        """ Change user status based on macaddress value """
        id_value = self.find_id_by_mac(macaddress)
        if id_value is not None: 
            index = self.__find_index_by_id__(id_value)
            self.data[index]["status"] = status
    
    def reset_user_statuses(self):
        for entry in self.data:
            entry["status"] = "offline"
            
    def add_user(self, nick, macaddress):
        self.cursor.execute('INSERT INTO users(mac,nick) VALUES(?,?)',
                            (macaddress, nick))
        self.database.commit()
        # Add record into data attribute
        last_row_id = self.cursor.lastrowid
        self.cursor.execute('SELECT * FROM users WHERE id = ?',
                            (last_row_id, ))
        entry = self.cursor.fetchone()
        t = dict(zip(self.columns, entry))
        t['status'] = 'offline'
        self.data.append(t)
        
    def delete_row(self, id_value):
        self.cursor.execute('DELETE FROM users WHERE id=?',
                            (id_value, ))
        self.database.commit()
        # Remove record in data attribute
        index = self.__find_index_by_id__(id_value)
        del self.data[index]
    
    def edit_row(self, id_value, new_mac, new_nick):
        # Update sqlite database
        self.cursor.execute('UPDATE users SET mac = ? ,nick = ? WHERE id= ?',
                                            (new_mac, new_nick, id_value))
        self.database.commit()
        # Update internal dictionary
        index = self.__find_index_by_id__(id_value)
        self.data[index]["mac"] = new_mac
        self.data[index]["nick"] = new_nick
                                            

"""
USER_DB = 'macdatabase.db'
db = MacTable(USER_DB)
print db.find_user_by_mac('a0:32:99:bd:11:a7')
print db.data
"""






