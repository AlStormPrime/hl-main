Configuration
=============

Currently config is stored in YAML file. Example of config:

```
# Example config file
---
ftp:
  host: foo.ftp.tools
  user: foo_ftp
  password: password
mqtt:
  server: server.com
  port: 16860
  user: user
  password: password
  visitors-topic: presence/visitors
wordpress:
  folder: /takoe.xyz/www/files
logging: spaceapi.log
data: spaceapi.json
```

By default, config file name is `spaceapi_config.yml`
