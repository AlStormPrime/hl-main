#!/usr/bin/env python

import json
import ftplib
import io
import yaml
import paho.mqtt.client as mqtt
import logging

CONFIG_FILE = "spaceapi_config.yml"
try:
    with open(CONFIG_FILE, 'r') as ymlfile:
        config = yaml.load(ymlfile, Loader=yaml.BaseLoader)
except IOError as e:
    logging.error("Config file not found!")
    logging.error("Exception: %s" % str(e))
    sys.exit(1)

logging.basicConfig(filename=config['logging'], level=logging.INFO)

DATA_FILE = config['data']
try:
  with open(DATA_FILE, "r") as read_file:
      data = json.load(read_file)
except IOError as e:
    logging.error("Data file not found!")
    logging.error("Exception: %s" % str(e))
    sys.exit(1)

def send_data(data):
    datastring = json.dumps(data)
    bio = io.BytesIO(datastring.encode())
    session = ftplib.FTP(config['ftp']['host'], config['ftp']['user'], config['ftp']['password'])
    session.cwd(config['wordpress']['folder'])
    session.storbinary('STOR spaceapi.json', bio)     
    session.quit()

def on_mqtt_connect(client, userdata, flags, rc):
    if rc == 0:
        logger.info("Connection successful")
    elif rc == 1:
        logger.warning("Connection refused - incorrect protocol version")
    elif rc == 2:
        logger.warning("Connection refused - invalid client identifier")
    elif rc == 3:
        logger.warning("Connection refused - server unavailable")
    elif rc == 4:
        logger.warning("Connection refused - bad username or password")
    elif rc == 5:
        logger.warning("Connection refused - not authorised")
    else:
        logger.warning("Unknown connection error: %d", rc)
    client.subscribe(config['mqtt']['topic'])
    logger.info("Subscribed to %s topic", config['mqtt']['topic'])
    
def on_mqtt_disconnect(client, userdata, rc):
    logger.info("MQTT disconnect occured. Result code %d", rc)
    if rc != 0:
        logger.warning("Unexpected disconnection.")
    else:
        logger.info("Disconnection occured as responce to disconnect() call")

def on_mqtt_message(client, userdata, message):
    visitors = message.payload.decode("utf-8")
    is_open = len(visitors) > 0
    data['state']['open'] = is_open
    send_data(data)
  
mqtt_client = mqtt.Client()             
mqtt_client.on_connect = on_mqtt_connect  
mqtt_client.on_message = on_mqtt_message
mqtt_client.username_pw_set(config['mqtt']['user'], config['mqtt']['password'])
mqtt_client.connect(config['mqtt']['host'], int(config['mqtt']['port']), 60)
mqtt_client.on_disconnect = on_mqtt_disconnect
      
mqtt_client.loop_start() 
