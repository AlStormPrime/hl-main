# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 22:39:48 2016

@author: artsin
"""


from datetime import datetime, date, MINYEAR, MAXYEAR


def parse_log(input_log, date_start=None, date_end=None, verbose=False):    
    """
    Input date example: 2016-09-18
    
    Usage example:
    >>> parse_log("mylog", "2016-10-10", "2016-12-10")
    """
    summary = dict()
    # Filling date boundaries
    if date_start:
        date_start = datetime.strptime(date_start, "%Y-%m-%d")
    else:
        date_start = datetime(MINYEAR, 1, 1, 0, 0, 0)
        
    if date_end:
        date_end = datetime.strptime(date_end, "%Y-%m-%d")
    else:
        date_end = datetime(MAXYEAR, 1, 1, 0, 0, 0)
    
    #Parse log file
    with open(input_log, 'r') as f:
        start_time = None
        start_owner = None
        start_stop_mode = None
        for line in f.readlines():
            line_words = line.split()        
            try:          
                datetime_string = ' '.join(line_words[1:3])
                date_object = datetime.strptime(datetime_string, "%Y-%m-%d %H:%M:%S")
                #print line_words[3], start_stop_mode
                if line_words[3] == '1':
                    start_time = date_object
                    start_owner = line_words[0]
                elif (line_words[3] == '0') and (start_stop_mode == '1'):
                    if date_start < start_time < date_end:                        
                        # Flush data when '1' switches to '0'
                        consumed_time = (date_object-start_time)                        
                        try:
                            #Try to add if key exists
                            summary[start_owner] += consumed_time
                        except KeyError:
                            #Create new key if not exists
                            summary[start_owner] = consumed_time
                        if verbose != False:
                            print datetime_string, "Name:", start_owner, "Consumed time: ", consumed_time 
                start_stop_mode = line_words[3]
            except ValueError:
                pass
    return summary

def get_monthtime_for_user(logfile, username):
    now = datetime.now()
    
    monthstart = now.strftime("%Y-%m-01")
    nowdate = now.strftime("%Y-%m-%d")
    result = parse_log(logfile, monthstart)
    
    return result[username]


#print get_monthtime_for_user("/home/pi/machinelog.txt", "ostapsky")