import socket
import sqlite3 as sqlite
from time import localtime, strftime
from datetime import timedelta
import log_parser
from slacker import Slacker
import sys
import yaml

if len(sys.argv) == 1:
   print "Tool name is not provided!" 
   exit(1)

CONFIG_FILE = 'config.cfg'
cfg = yaml.load(open(CONFIG_FILE, 'r'))
DOORBOT_TOKEN = cfg['doorbot-token']
VISITORS_DB = cfg['visitors-database-path']
LATEST_KEY_FILE = cfg['latest-key-file']
 
tool_name = sys.argv[1]
if not cfg.has_key(tool_name):
    print "Tool name unknown", tool_name
    exit(2)

tool = cfg[tool_name]
server_name = tool['server_name']
server_port = int(tool['server_port'])
db_index = int(tool['db_index'])       
log_filename = tool['log_filename']

slack = Slacker(DOORBOT_TOKEN)

def report_user_time(u):
    used_time = log_parser.get_monthtime_for_user(log_filename, u)
    max_time = timedelta(hours=12, minutes=0, seconds=0)
    if used_time < max_time:
        try:
            slack.chat.post_message('@%s'%u, 'You have used %s of %s time this month'%(used_time, tool_name), as_user=True)
        except:
            slack.chat.post_message('#tools_time', '%s used %s of %s time this month'%(u, tool_name), as_user=True)
    else:
        try:
            slack.chat.post_message('@%s'%u, 'ACHTUNG ALARM! you have used %s of %s time this month, STAHP!'%(used_time, tool_name), as_user=True)
            slack.chat.post_message('#tools_time', '%s used %s of %s time this month'%(u, used_time, tool_name), as_user=True)
        except:
            slack.chat.post_message('#tools_time', '%s used %s of %s time this month'%(u, used_time, tool_name), as_user=True)


def get_user(key):
  conn = sqlite.connect(VISITORS_DB)
  c = conn.cursor()
  query="SELECT * FROM users WHERE key='%s'"%(key)
  c.execute(query)
  user = c.fetchone()  
  conn.commit()
  conn.close()
  return user

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# Bind the socket to the address given on the command line
server_address = (server_name, server_port)
sock.bind(server_address)
sock.listen(1)

state = 0
report = False

while True:
    connection, client_address = sock.accept()

    try:
        while True:
            data = connection.recv(16)
            if data:           
                f = open(LATEST_KEY_FILE, "w")
                f.write("%s"%data)
                f.close()

                u = get_user(data[4:10])

                if u!= None: 
				   
                   if u[db_index] == 1:
                        if state == 0:
                            s = "%-20s %s 1\n"%(u[1], strftime("%Y-%m-%d %H:%M:%S", localtime())) 
                            connection.sendall("1")
                            state = 1
                        else: 
                            s = "%-20s %s 0\n"%(u[1], strftime("%Y-%m-%d %H:%M:%S", localtime()))						
                            connection.sendall("0")
                            state = 0
                            report = True    
                   else:
                        s = "%-20s %s 0\n"%(u[1], strftime("%Y-%m-%d %H:%M:%S", localtime()))
                        connection.sendall("0")
                        state = 0;
                else:
                   s = "%-20s %s 0\n"%('unknown',strftime("%Y-%m-%d %H:%M:%S", localtime()))
                   connection.sendall("0")
                   state = 0;
		

                print(s)	
                with open(log_filename, "a") as f:
                    f.write(s)
                    f.close()
                    
                if report:
                   report_user_time(u[1])
                   report = False
                   
            else:
                break
            
    except:
        pass
        
    finally:
        connection.close()
        
