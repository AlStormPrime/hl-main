#! /usr/bin/python
#

import serial
from slacker import Slacker
import sqlite3 as sqlite
import struct 
import mraa
import time 
import yaml

CONFIG_FILE = 'config.cfg'
cfg = yaml.load(open(CONFIG_FILE, 'r'))
DOORBOT_TOKEN = cfg['doorbot-token']
VISITORS_DB = cfg['visitors-database-path']
LATEST_KEY_FILE = cfg['latest-key-file']

slack = Slacker(DOORBOT_TOKEN)

pin = mraa.Gpio(43)    
pin.dir(mraa.DIR_OUT) # set as OUTPUT pin  
pin.write(1)

def getUser(key):
  conn = sqlite.connect(VISITORS_DB)
  c = conn.cursor()
  query="SELECT * FROM users WHERE key='%s'"%(key)
  c.execute(query)
  user = c.fetchone()  
  conn.commit()
  conn.close()
  return user

ser = serial.Serial('/dev/ttyS0', 57600, timeout = 1)

while(1):
    try:	   
        serial_data = ser.read(3)
                
        if (serial_data != ''):
            code = struct.unpack(">I", '\x00' + serial_data)
            req = getUser("%06X" % code)
            
            with open(LATEST_KEY_FILE, "w") as f:
                f.write("%06X" % code)
                f.close()            
            
            if req != None:
                print req[1]
                if (req[3]):
                     pin.write(0)
                     time.sleep(1)
                     pin.write(1)
                try:     
                    slack.chat.post_message('#door-events', '%s entered'%req[1], as_user=True)
                except:
                    pass
       
    except KeyboardInterrupt:
        ser.close()
        break
              
       #except:
        #   print "exception"
       #    ser.close()
       #    ser = serial.Serial('/dev/ttyS0', 57600, timeout=1)
       

            

