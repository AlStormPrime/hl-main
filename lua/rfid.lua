PORT = 1234
ADDRESS = '192.168.1.115'

uart.setup(0,9600,8,0,1,0)

wait = 0

gpio.mode(2,gpio.OUTPUT)
gpio.write(2, gpio.LOW)


function post_data(postdata)
    connout = nil
    connout = net.createConnection(net.TCP, 0)
 
    connout:on("receive", function(connout, data)
		print(data)
		if data == '1' then
		    gpio.write(2, gpio.HIGH)
		else
		    gpio.write(2, gpio.LOW)
        end		
		connout:close();
    end)
 
    connout:on("connection", function(connout, payloadout) 
        connout:send(postdata)
    end)
 
    connout:on("disconnection", function(connout, payloadout)
        connout:close();
        collectgarbage();
    end)
 
    connout:connect(PORT,ADDRESS)
end

uart.on("data", '\003', 
	function(data)
		if string.len(data) == 14 then
			if wait == 0 then
				data0 = string.sub(data,2,5)
				data1 = string.sub(data,6,9)
				data2 = string.sub(data,10,13)
				num0 = tonumber(data0,16)
				num0 = bit.bxor(num0, 51085)
				num1 = tonumber(data1,16)
				num1 = bit.bxor(num1, 51085)
				num2 = tonumber(data2,16)
				num2 = bit.bxor(num2, 51085)

				s = string.format("%04X%04X%04X", num0, num1, num2)  
				post_data(s)
				uart.write(0, s)
				
			end
			tmr.alarm(0, 1500, 0, function() wait = 0 end)
			wait = 1
		end
	end
,0)
