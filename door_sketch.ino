#define INA 22
#define INB 23
#define LOCK 23
#define BITS_IN_CODE  26
#define RFID_TIMEOUT  1000

int prevA = 0;
int prevB = 0;

uint32_t code = 0;
uint32_t tempCode = 0;
uint32_t cnt = 0;
uint8_t bits = 0;
  
void setup() {
  Serial.begin(115200); // open serial connection to USB Serial port

  Serial1.begin(57600); // open internal serial connection to MT7688AN
                            // in MT7688AN, this maps to device
  pinMode(LOCK, OUTPUT);

  pinMode(INA, INPUT);
  pinMode(INB, INPUT);
  
  digitalWrite(LOCK, 1);
  
}

void loop() 
{
    int curA = digitalRead(INA);  
    if  (( curA != prevA ) && (curA > 0))  
    {
        tempCode <<= 1; 
        tempCode |= 1;
        bits++;
        cnt = RFID_TIMEOUT;
    }
    prevA = curA;

    int curB = digitalRead(INB);  
    if  (( curB != prevB ) && (curB > 0))  
    {
        tempCode <<= 1; 
        tempCode &= ~1;
        bits++;
        cnt = RFID_TIMEOUT;
    }
    prevB = curB;

    if (cnt) 
    {
      cnt--;
    }
    else
    {
     
      if (tempCode)
      {
         tempCode >>= (bits - BITS_IN_CODE);
         code = ( (tempCode >> 1) & 0xFFFFFF ) ^ 0xFFFFFF ^ 0xC78DC7;
         Serial1.write((uint8_t) ((code >> 16) & 0xFF) );
         Serial1.write((uint8_t) ((code >> 8) & 0xFF) );
         Serial1.write((uint8_t) ( code  & 0xFF) );
         tempCode = 0;
      }

       bits = 0;
    }

    delayMicroseconds(10);
    
}
