<?php
$servername = "hacklab.mysql.tools";
$username = ""; // Find in site settings.
$password = ""; // Find in site settings.
$dbname = "hacklab_presence";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// 10 minutes.
$sql = "SELECT name, seen FROM Users WHERE now() - seen < 600";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "Name: " . $row["name"]. " - Seen: " . $row["seen"], "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>
